<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class BriEcoplayApi
{
    protected $url;
    protected $appKey;
    protected $client;
    /**
     * Sikd constructor.
     */
    public function __construct(Client $client)
    {
        $this->url = env("APP_API");
        $this->api_url = $this->url;
        $this->client = $client;
    }


    private function generateApiUrl($url)
    {
        return $this->url . $url;
    }


    private function proceedException($e, $apiUrl)
    {
        $message = 'Unknown Error';
        $code = '000';
        if ($e instanceof TransferException) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $message = $response->getReasonPhrase();
                $code = $response->getStatusCode();
            }
        }
        Log::alert('ERROR API ==> ' . $message . ' at ' . $apiUrl);
        return new SimpleAPIResponse($code, $message . ' at ' . $apiUrl . '.');
    }

    private function parseResponse($response)
    {
        $body = $response->getBody();
        $code = $response->getStatusCode(); // 200
        $reason = $response->getReasonPhrase(); // OK
        if ($code == 200 && $reason == 'OK') {
            $result = \GuzzleHttp\json_decode($body);
            return new SimpleAPIResponse($code, $result);
        } elseif ($code == 201 && $reason == 'Created') {
            $result = \GuzzleHttp\json_decode($body);
            return new SimpleAPIResponse($code, $result);
        } elseif ($code == 401) {
            \Auth::logout();
            session()->flush();
            return new SimpleAPIResponse(401, 'Unauthorized');
        } else {
            return new SimpleAPIResponse(400, 'Bad API Result.');
        }
    }

    public function login($url, $params = [])
    {
        $apiUrl = $this->generateApiUrl($url);
        // dd($accessToken);
        $body = [
          'username' => $params->username,
          'password' => $params->password
        ];
        try {
            $response = Http::post($apiUrl, $body);
            Log::alert('Response ==> ' . $response);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;
    }

    public function post($url, $params = [])
    {
        $apiUrl = $this->generateApiUrl($url);
        $accessToken = session('userToken');
      // dd($accessToken);
        $headers = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'json' => $params
        ];
        try {
            $response = $this->client->request('POST', $apiUrl, $headers);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;
    }

    public function put($url, $params = [])
    {
        $apiUrl = $this->generateApiUrl($url);
        Log::alert('API URL ==> ' . $apiUrl);
        $accessToken = session('userToken');
      // dd($accessToken);
        $headers = [
            'Authorization' => 'Bearer ' . $accessToken
        ];
        
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken
            ])->put($apiUrl, $params);
            Log::alert('Response ==> ' . $response);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;
    }

    public function patch($url, $params = [])
    {
        $apiUrl = $this->generateApiUrl($url);
        Log::alert('API URL ==> ' . $apiUrl);
        $accessToken = session('userToken');
        //dd($accessToken);
        $headers = [
            'Authorization' => 'Bearer ' . $accessToken
        ];
        
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-Type' => 'application/json',
            ])->patch($apiUrl, $params);
            Log::alert('Response ==> ' . $response);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;
    }

    public function get($url)
    {
        $apiUrl = $this->generateApiUrl($url);
        Log::alert('API URL ==> ' . $apiUrl);
        $accessToken = session('userToken');
        $headers = [
            'Authorization' => 'Bearer ' . $accessToken,
        ];
        try {
            $response = Http::withHeaders($headers)->get($apiUrl);
            Log::alert('Response ==> ' . $response);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;
    }



    public function delete($url)
    {
        $apiUrl = $this->generateApiUrl($url);
        Log::alert('API URL ==> ' . $apiUrl);
        $accessToken = session('userToken');
        $headers = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ];
        try {
            $response = $this->client->request('delete', $apiUrl, $headers);
            $parsedResponse = $this->parseResponse($response);
        } catch (\Exception $e) {
            $parsedResponse = $this->proceedException($e, $apiUrl);
        }
        return $parsedResponse;

    }

    protected function set_token_api($data)
    {
        Session::put('userToken', $data);
    }

}
