<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use stdClass;

class AdministratorController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/admins?page='.$page.'&limit='.$limit.'&status=0');
            $dataAdmin = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataAdmin->admin)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataAdmin->admin;
            $response->data->original['recordsFiltered'] = $dataAdmin->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataAdmin->pagination->total_results;
            $response->recordsFiltered = $dataAdmin->pagination->total_results;
            $response->recordsTotal = $dataAdmin->pagination->total_results;
            return $response;
        }
        return view('administrator.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id)
    {
        $payload = [
            'status' => (int) $request->status
        ];
        Log::debug(json_encode($payload));
        $response = $this->BriEcoplayApi->put('/admins/status/'.$id, $payload);
        $status = $response->status;

        echo $status;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = [
            'username' => $request->username,
            'full_name' => $request->full_name,
            'email' => $request->email,
            'password' => $request->password
        ];
        Log::debug(json_encode($payload));
        $response = $this->BriEcoplayApi->post('/register_admin', $payload);
        Log::debug(json_encode($response));

        return redirect()->route('administrator.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('administrator.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
