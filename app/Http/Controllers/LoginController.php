<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use stdClass;
use Log;

class LoginController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    public function dashboard(){
        // $getToken = Session::get('userToken');

        // if ($getToken == null) {
        //     return redirect('/');
        // }
        return view('dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Default username.
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Storing PHP token in session.
     */
    protected function set_token_api($data)
    {
        Session::put('userToken', $data->token);
        Session::put('userName', $data->name);
        Session::put('userRole', $data->role);
    }

    /**
     * Login function redirect users after login.
     */
    protected function login(Request $request)
    {   
        $req = new stdClass();
        $req->username = $request->username;
        $req->password = $request->password;

        $response = $this->BriEcoplayApi->login('/login_admin', $req);

        $status = $response->status;

        $this->set_token_api($response->result->data);
        
        return redirect()->route('dashboard');
    }

    /**
     * Login function redirect users after login.
     */
    protected function logout(Request $request)
    {   
        Session::forget('userToken');
        Session::forget('userName');
        Session::forget('userRole');
        
        return redirect('/');
    }

}
