<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use stdClass;


class WaGroupController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/wa-groups?page=' . $page . '&limit=' . $limit);
            $dataWa = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataWa->Wa)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataWa->Wa;
            $response->data->original['recordsFiltered'] = $dataWa->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataWa->pagination->total_results;
            $response->recordsFiltered = $dataWa->pagination->total_results;
            $response->recordsTotal = $dataWa->pagination->total_results;
            return $response;
        }

        return view('wa_group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload = [
                    'name' => $request->name,
                    'link' => $request->link,
                    'member' => (int) $request->member,
                    'image' => 'data:image/png;base64,'.$image
                ];
                
                $response = $this->BriEcoplayApi->post('/wa-group', $payload);
                $status = $response->status;

                if ($status == 200) {
                    return back()->with('success', 'Succesfully Added');
                }
            }

        }

        return back()->with('failed', 'Failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = $this->BriEcoplayApi->get('/wa-groups/'.$id);
        $dataWa = $response->result->data->Wa;

        return view('wa_group.edit', compact('dataWa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = [
            'name' => $request->name,
            'link' => $request->link,
            'member' => (int) $request->member
        ];

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload['image'] = 'data:image/png;base64,' . $image;

            }
        }

        
        $response = $this->BriEcoplayApi->patch('/wa-group/'.$id, $payload);
        $status = $response->status;

        if ($status == 200) {
            return redirect()->route('wa-group.index')->with('success', 'Succesfully Edited');
        }
        
        return back()->with('failed', 'Failed');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id)
    {
        $payload = [
            'status' => (int) $request->status
        ];
        
        $response = $this->BriEcoplayApi->patch('/banner/'.$id, $payload);
        $status = $response->status;

        echo $status;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
