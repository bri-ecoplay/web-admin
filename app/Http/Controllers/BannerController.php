<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use stdClass;


class BannerController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/list/banners?page=' . $page . '&admin_id=1&limit=' . $limit);
            $dataBanner = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataBanner->Banner)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataBanner->Banner;
            $response->data->original['recordsFiltered'] = $dataBanner->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataBanner->pagination->total_results;
            $response->recordsFiltered = $dataBanner->pagination->total_results;
            $response->recordsTotal = $dataBanner->pagination->total_results;
            return $response;
        }

        return view('banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload = [
                    'admin_id' => 1,
                    'image' => 'data:image/png;base64,'.$image,
                    'name' => $request->name,
                    'status' => 0
                ];
                
                $response = $this->BriEcoplayApi->post('/banner', $payload);
                $status = $response->status;

                if ($status == 200) {
                    return back()->with('success', 'Succesfully Added');
                }
            }

        }

        return back()->with('failed', 'Failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = $this->BriEcoplayApi->get('/banners/' . $id);
        $dataBanner = $response->result->data->banner;

        return view('banner.edit', compact('dataBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = [
            'name' => $request->name,
            'admin_id' => 1,
        ];

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload['image'] = 'data:image/png;base64,' . $image;
            }
        }


        $response = $this->BriEcoplayApi->patch('/banner/' . $id, $payload);
        $status = $response->status;

        if ($status == 200) {
            return redirect()->route('banner.index')->with('success', 'Succesfully Edited');
        }

        return back()->with('failed', 'Failed');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request, $id)
    {
        $payload = [
            'status' => (int) $request->status,
            'admin_id' => 0,
        ];
        Log::debug(json_encode($payload));
        $response = $this->BriEcoplayApi->patch('/banner/'.$id, $payload);
        $status = $response->status;

        echo $status;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
