<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use stdClass;

class UserController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        Log::alert('session ==> ' . Session::get('userName'));



        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/user/list?page='.$page.'&limit='.$limit.'&status=1');
            $dataNasabah = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataNasabah->nasabah)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataNasabah->nasabah;
            $response->data->original['recordsFiltered'] = $dataNasabah->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataNasabah->pagination->total_results;
            $response->recordsFiltered = $dataNasabah->pagination->total_results;
            $response->recordsTotal = $dataNasabah->pagination->total_results;
            return $response;
        }
        return view('user.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataDashboard(Request $request)
    {   
        if ($request->ajax()) {
            $response = $this->BriEcoplayApi->get('/dashboard_data');
            $dataDashboard = $response->result->data;
            $response = new stdClass();
            Log::debug(json_encode($dataDashboard->dashboard_data));
            $response->data = $dataDashboard->dashboard_data;
            return $response;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/user/list?page='.$page.'&limit='.$limit.'&status=0');
            $dataNasabah = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataNasabah->nasabah)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataNasabah->nasabah;
            $response->data->original['recordsFiltered'] = $dataNasabah->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataNasabah->pagination->total_results;
            $response->recordsFiltered = $dataNasabah->pagination->total_results;
            $response->recordsTotal = $dataNasabah->pagination->total_results;
            return $response;
        }
        return view('user.create');
    }

    public function acceptUser($id)
    {
        $payload = [
            "id" => (int)$id
        ];
        $response = $this->BriEcoplayApi->put('/user/verification',$payload);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = $this->BriEcoplayApi->get('/user/detail/'.$id);
        $detailNasabah = $response->result->data->user_data;
        return view('user.show', compact('detailNasabah', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->BriEcoplayApi->delete('/user/'.$id.'/delete');
        Log::debug(json_encode($response->result->code));
        if ($response->result->code != 201) {
            return redirect()->route('user.detail', $id);
        }
    }

    public function detail($id)
    {
        $response = $this->BriEcoplayApi->get('/user/detail/'.$id);
        $detailNasabah = $response->result->data->user_data;
        return view('user.detail', compact('detailNasabah', 'id'));
    }
}
