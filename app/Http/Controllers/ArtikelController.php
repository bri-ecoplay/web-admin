<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\BriEcoplayApi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use stdClass;

class ArtikelController extends Controller
{
    protected $BriEcoplayApi;
    protected $errorAPI;

    public function __construct(BriEcoplayApi $BriEcoplayApi)
    {
        $this->BriEcoplayApi = $BriEcoplayApi;
        $this->errorAPI = 'API error : ';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $page = ($request->start / $request->length) + 1;
            $limit = (string)$request->length;
            $response = $this->BriEcoplayApi->get('/news?page=' . $page . '&limit=' . $limit);
            $dataArtikel = $response->result->data;
            $response = new stdClass();
            $response->data = datatables($dataArtikel->news)->toJson();
            $response->draw = $response->data->original['draw'];
            $response->data->original['data'] = $dataArtikel->news;
            $response->data->original['recordsFiltered'] = $dataArtikel->pagination->total_results;
            $response->data->original['recordsTotal'] = $dataArtikel->pagination->total_results;
            $response->recordsFiltered = $dataArtikel->pagination->total_results;
            $response->recordsTotal = $dataArtikel->pagination->total_results;
            return $response;
        }

        return view('artikel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload = [
                    'admin_id' => 1,
                    'title' => $request->title,
                    'content' => $request->content,
                    'status' => 0,
                    'thumbnail' => 'data:image/png;base64,' . $image
                ];

                $response = $this->BriEcoplayApi->post('/news', $payload);
                $status = $response->status;
                // dd($response);
                if ($status == 200) {
                    return back()->with('success', 'Succesfully Added');
                }
            }
        }

        return back()->with('failed', 'Failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = $this->BriEcoplayApi->get('/news/' . $id);
        $dataNews = $response->result->data->news;

        return view('artikel.edit', compact('dataNews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payload = [
            'title' => $request->title,
            'content' => $request->content,
        ];

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $image = base64_encode(file_get_contents($request->file('image')));
                } catch (FileNotFoundException $e) {
                    abort(404); //or whatever you want do here
                }

                $payload['thumbnail'] = 'data:image/png;base64,' . $image;
            }
        }

        $response = $this->BriEcoplayApi->patch('/news/' . $id, $payload);
        $status = $response->status;

        if ($status == 200) {
            return redirect()->route('artikel.index')->with('success', 'Succesfully Edited');
        }

        return back()->with('failed', 'Failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
