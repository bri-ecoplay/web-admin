@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Event</h5>
            </div>

            <div class="col-12">
                <div class="note">
                    <p class="note-text"><span>Catatan : </span>Silakan upload gambar banner dengan ukuran 308 x 128, dengan format JPG, JPEG dan PNG,</p>
                </div>

                <form action="">
                    <div class="create-event mt-20">
                        <div class="row">
                            <div class="col-2">
                                <label for="input-file">
                                    <div class="input-img">
                                        <img src="{{ asset ('svg/img.svg')}}" alt=""> 
                                        <p class="upload-text">Upload</p>
                                    </div>
                                </label>
                            </div>
                            <!-- hidden button -->
                            <div class="col-2" style="margin-top: 20px;">
                                <input type="file" class="input" id="input-file">
                            </div>
                        </div>   
                        <div class="row">                            
                            <label for="" class="form-label" style="margin-top: 20px;">Title Event</label>
                            <input type="text" class="input-text" id="" placeholder="Contoh : Promo Pengguna Baru">
                        </div>
                        <div class="row">                            
                            <label for="" class="form-label" style="margin-top: 20px;">Link Event</label>
                            <input type="text" class="input-text" id="" placeholder="Contoh : promo.bri.co.id/produc/..">
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>

        <!-- List Banner Section -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500 mt-70">List Banner</h5>
            </div>
            <div class="col-lg-12">
                <div class="mt-20">
                    <table class="table table-striped table-borderless" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center py-3">No</th>
                                <th class="py-3">Update</th>
                                <th class="py-3">Banner</th>
                                <th class="py-3">Link</th>
                                <th class="py-3">On/Off</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>12 Jul 2021 ; 12:40</td>
                                <td>Promo UMKM</td>
                                <td>https://getbootstrap.com/docs/5.1/components/dropdowns/</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td>12 Jul 2021 ; 12:40</td>
                                <td>Promo UMKM</td>
                                <td>https://getbootstrap.com/docs/5.1/components/dropdowns/</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td>12 Jul 2021 ; 12:40</td>
                                <td>Promo UMKM</td>
                                <td>https://getbootstrap.com/docs/5.1/components/dropdowns/</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable();
        });
    </script>
@endsection