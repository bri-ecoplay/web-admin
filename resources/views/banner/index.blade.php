@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Banner</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <div class="note">
                    <p class="note-text"><span>Catatan : </span>Silakan upload gambar banner dengan ukuran 308 x 128, dengan format JPG, JPEG dan PNG,</p>
                </div>

                <form action="{{ url('/banner') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="create-event mt-20">
                        <div class="row">
                            <div class="col-2">
                                <label for="input-file">
                                    <div class="input-img">
                                        <img src="{{ asset ('svg/img.svg')}}" alt="">
                                        <p class="upload-text">Upload</p>
                                    </div>
                                </label>
                            </div>
                            <!-- hidden button -->
                            <div class="col-2" style="margin-top: 20px;">
                                <input type="file" class="input" id="input-file" name="image">
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Title Banner</label>
                            <input type="text" class="input-text" id="" name="name" placeholder="Contoh : Banner Baru">
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>

        <!-- List Banner Section -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500 mt-70">List Banner</h5>
            </div>
            <div class="col-lg-12">
                <div class="mt-20">
                    <table class="table table-striped table-borderless" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center py-3">No</th>
                                <th class="py-3">Update</th>
                                <th class="py-3">Banner</th>
                                <th class="py-3" style="width:30%">Image</th>
                                <th class="py-3">On/Off</th>
                                <th class="py-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>12 Jul 2021 ; 12:40</td>
                                <td>Promo UMKM</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    function updateStatus(id, status) {
        console.log(id);
        let url = "{{ route('banner.update-status', 'id') }}";
        url = url.replace('id', id);
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                _token: "{{ csrf_token() }}",
                status: status
            },
            success: function(Resp) {
                alert('Status berhasil diubah!')
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
        return false;
    }

    function formatDate(date) {
        const transMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'];

        let d = new Date(date);

        let month = transMonth[d.getMonth()],
            day = d.getDate(),
            year = d.getFullYear(),
            hours = d.getHours(),
            minutes = d.getMinutes();

        if (day.length < 2)
            day = '0' + day;

        return `${year} ${month} ${day} ; ${hours} : ${minutes}`;
    }

    $(document).ready(function() {
        $('#dataTable').DataTable({
            "pageLength": 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('banner.index') }}",
                dataSrc: 'data.original.data'
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: "date",
                    name: "date",
                    "render": function(data, type, full) {
                        let date = formatDate(data)
                        return date;
                    }
                },
                {
                    data: 'name',
                    name: 'name',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'image',
                    name: 'image',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return `<img src="${data}" atl="banner" width="100%" class="image">`;
                    }
                },
                {
                    data: 'status',
                    name: 'status',
                    "render": function(data, type, full) {
                        let status = `<div class="form-check form-switch" onclick="updateStatus(${full.id}, ${data?'0':'1'})">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" ${data?'checked':''}>
                                      </div>`;
                        return status;
                    }
                },
                {
                    data: 'id',
                    name: 'action',
                    "render": function(data, type, full) {
                        let action = `<a href="banner/${data}/edit" class="btn btn-gray d-inline">
                                        Edit
                                    </a>`;
                        return action;
                    }
                }
            ]
        });
    });
</script>
@endsection