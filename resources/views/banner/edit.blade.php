@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Banner</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <div class="note">
                    <p class="note-text"><span>Catatan : </span>Silakan upload gambar banner dengan ukuran 308 x 128, dengan format JPG, JPEG dan PNG,</p>
                </div>

                <form action="{{ url('/banner', $dataBanner->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="create-event mt-20">
                        <div class="row">
                            <div class="col-2">
                                <label for="input-file">
                                    <div class="input-img">
                                        <img src="{{ $dataBanner->image }}" alt="">
                                        <p class="upload-text">Upload</p>
                                    </div>
                                </label>
                            </div>
                            <!-- hidden button -->
                            <div class="col-2" style="margin-top: 20px;">
                                <input type="file" class="input" id="input-file" name="image">
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Title Banner</label>
                            <input type="text" class="input-text" id="" name="name" value="{{ $dataBanner->name??'' }}">
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function() {

    });
</script>
@endsection