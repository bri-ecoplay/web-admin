@extends('layout.layout')

@section('title', 'Administrator')

@section('content')
    <div class="container mt-5">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h5 class="font-weight-500">Administrator</h5>
            </div>
            <div class="col-lg-6">
                <a href="{{  route('administrator.create') }}" class="btn text-primary border-1 border-primary bg-transparent">+ Tambah Admin</a>
            </div>
        </div>
        <hr class="my-3">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped table-borderless" id="dataTable">
                    <thead>
                        <tr>
                            <th class="text-center py-3">No</th>
                            <th class="py-3">Nama Admin</th>
                            <th class="py-3">Username</th>
                            <th class="py-3">Email</th>
                            <th class="py-3">Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function updateStatus(id, status) {
        console.log(id);
        let url = "{{ route('admin.update-status', 'id') }}";
        url = url.replace('id', id);
        $.ajax({
            type: 'PUT',
            url: url,
            data: {
                _token: "{{ csrf_token() }}",
                status: status
            },
            success: function(Resp) {
                alert('Status berhasil diubah!')
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
        return false;
    }

        $(document).ready(function(){
            $('#dataTable').DataTable({
                "pageLength": 10,
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('administrator.index') }}",
                    dataSrc: 'data.original.data'
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: "full_name",
                        name: "full_name",
                        "render": function(data, type, full) {
                            if (!data)
                                return '-';
                            return data;
                        }
                    },
                    {
                        data: 'username',
                        name: 'username',
                        "render": function(data, type, full) {
                            if (!data)
                                return '-';
                            return data;
                        }
                    },
                    {
                        data: 'email',
                        name: 'email',
                        "render": function(data, type, full) {
                            if (!data)
                                return '-';
                            return data;
                        }
                    },
                    {
                        data: 'status',
                        name: 'status',
                        "render": function(data, type, full) {
                            let status = `<div class="form-check form-switch" onclick="updateStatus(${full.id}, ${data?'0':'1'})">
                                            <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" ${data?'checked':''}>
                                        </div>`;
                            return status;
                        }
                    }
                ]
            });
        });
    </script>
@endsection