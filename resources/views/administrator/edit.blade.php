@extends('layout.layout')

@section('title', 'Administrator')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6">
                <h5 class="font-weight-500">Detail Admin</h5>
            </div>
        </div>
        <hr class="my-3">
        <div class="row">
            <p style="font-weight: bold">Data Personal</p>
            <div class="col-lg-12">
                <table class="table table-borderless">
                    <tr>
                        <td>Nama</td>
                        <td>testes</td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td>testes</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>testes</td>
                    </tr>
                    <tr>
                        <td>Nomor HP</td>
                        <td>testes</td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            <div class="form-group row">
                                <div class="col-4">
                                    <input type="password" class="form-control">    
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr class="my-5">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('administrator.update', 1) }}" class="btn btn-primary btn-lg">Ubah</a>
            </div>
        </div>
    </div>
@endsection