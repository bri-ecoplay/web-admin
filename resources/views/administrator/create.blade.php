@extends('layout.layout')

@section('title', 'Administrator')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6">
                <h5 class="font-weight-500">Tambah Admin</h5>
            </div>
        </div>
        <hr class="my-3">
        <form action="{{ route('administrator.store') }}" method="POST" >
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group row">
                    
                        <div class="col-lg-4">
                            
                            <label for="" class="form-control-label">Nama</label>
                            <input type="text" name="full_name" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="form-group mt-4 row">
                        <div class="col-lg-4">
                            <label for="" class="form-control-label">Username</label>
                            <input type="text" name="username" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="form-group mt-4 row">
                        <div class="col-lg-4">
                            <label for="" class="form-control-label">Email</label>
                            <input type="text" name="email" class="form-control form-control-lg">
                        </div>
                    </div>
                    <!-- <div class="form-group mt-4 row">
                        <div class="col-lg-4">
                            <label for="" class="form-control-label">No HP</label>
                            <input type="text" class="form-control form-control-lg">
                        </div>
                    </div> -->
                    <div class="form-group mt-4 row">
                        <div class="col-lg-4">
                            <label for="" class="form-control-label">Password</label>
                            <input type="password" name="password" class="form-control form-control-lg">
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-5">
            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-primary btn-lg" id="submit">SIMPAN</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    
@endsection