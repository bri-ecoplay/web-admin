@extends('layout.layout')
@section('title','Dashboard')
@section('content')
<section>
    <div class="container">
        <div class="tabs">
            <!-- indicator -->
            <div id="card-indicator">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <button class="tablinks active" onclick="openCity(event, 'newUser')">
                                    <p>Pengguna Baru</p>                    
                                    <h1 id="newUsers" data-myval="0">0</h1>
                                </button>
                            </div>
                            <div class="col-lg-3">
                                <button class="tablinks" onclick="openCity(event, 'jumlahToko')">
                                    <p>Jumlah Toko</p>
                                    <h1 id="totalStore" data-myvalStore="0">0</h1>
                                </button>
                            </div>
                            <div class="col-lg-3">
                                <button class="tablinks" onclick="openCity(event, 'jumlahProduk')">
                                    <p>Jumlah Produk</p>
                                    <h1 id="totalProduct" data-myvalStore="0">0</h1>
                                </button>
                            </div>
                            <div class="col-lg-3">
                                <button class="tablinks" onclick="openCity(event, 'totalUser')">
                                    <p>Total Pengguna</p>
                                    <h1 id="totalUsers" data-myvalStore="0">0</h1>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr class="my-4 bg-transparent">
            
            <!-- pengajuan baru -->
            <div id="newUser"  class="tabcontent" style="display: block;">
                <h1 class="fs-5 fw-bold mb-4">Pengajuan Baru</h1>

                <div class="table-responsive">
                    <table class="table table-striped table-borderlesss" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center py-3">No</th>
                                <th class="py-3">Nama Nasabah</th>
                                <th class="py-3">Jenis Usaha</th>
                                <th class="py-3">Rantai Usaha</th>
                                <th class="py-3">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div id="jumlahToko" class="tabcontent">
                <h1 class="fs-5 fw-bold mb-4">Jumlah Toko</h1>
                <canvas id="jumlahTokoChart " style="width:800px;max-width:100%;height:400px"></canvas>
            </div>
            <div id="jumlahProduk" class="tabcontent">
                <h1 class="fs-5 fw-bold mb-4">Jumlah Produk</h1>
                <canvas id="jumlahProdukChart" style="width:800px;max-width:100%;height:400px"></canvas>
            </div>
            <div id="totalUser" class="tabcontent">
                <h1 class="fs-5 fw-bold mb-4">Jumlah Produk</h1>
                <canvas id="totalUserChart" style="width:800px;max-width:100%;height:400px"></canvas>
            </div>
        </div>
        <div id="jumlahToko" class="tabcontent" style="display: block">
            <h1 class="fs-5 fw-bold mb-4">Jumlah Toko</h1>
        </div>
        <div id="jumlahProduk" class="tabcontent" style="display: block">
            <h1 class="fs-5 fw-bold mb-4">Jumlah Produk</h1>
        </div>
        <div id="totalPengguna" class="tabcontent" style="display: block">
            <h1 class="fs-5 fw-bold mb-4">Total Pengguna</h1>
        </div>
    </div>
</section>
@endsection

@section('script')
    <style>
        canvas {
           background-color: rgba(255, 255, 255, 1);
        }
    </style>
    
    <script>
        var xDateStore = [];
        var yDataStore = [];
        var xDateProduct = [];
        var yDataProduct = [];
        var xDateUser = [];
        var yDataUser = [];

        var data_dashboard = function () {
            var tmp = null;
            $.ajax({
                async: false,
                global: false,
                data: { 'request': "", 'target': 'arrange_url', 'method': 'method_target' },
                url: "{{ route('user.dataDashboard') }}",
                success: function (response) {
                    tmp = response.data;
                }
            });
            return tmp;
        }();
        
        xDateStore = data_dashboard.date_store
        yDataStore = data_dashboard.data_store
        xDateProduct = data_dashboard.date_product
        yDataProduct = data_dashboard.data_product
        xDateUser = data_dashboard.date_user
        yDataUser = data_dashboard.data_user
        
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "{{ route('user.create') }}",
                data: "?start=0&length=15",
                success: function(response){
                    console.log(response.recordsTotal);
                    document.getElementById("newUsers").innerHTML = response.recordsTotal;
                }
            });

           $.ajax({
                url: "{{ route('user.dataDashboard') }}",
                success: function(response){
                    document.getElementById("totalProduct").innerHTML = response.data.total_product;
                    document.getElementById("totalStore").innerHTML = response.data.total_store;
                    document.getElementById("totalUsers").innerHTML = response.data.total_user;
                }
            });

            function totalUser(){
                return $.ajax({
                    url: "{{ route('user.create') }}",
                    data: "?start=0&length=15",
                    success: function(response){
                        console.log(response.recordsTotal);
                        document.getElementById("newUser").innerHTML = response.recordsTotal;
                    }
                });
            }

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.create') }}",
                    dataSrc: 'data.original.data'
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'business_type', name: 'business_type' },
                    { data: 'business_chain', name: 'business_chain' },
                    { 
                        data: 'id',
                        name: 'action',
                        "render": function(data, type, full) {
                            let status = `<button class="btn btn-gray d-inline">
                                                <i class="bi bi-x-lg text-danger declineUser" data-id="`+data+`" href="javascript:void(0);"></i>
                                            </button>
                                            <button class="btn btn-green d-inline acceptUser" data-id="`+data+`" href="javascript:void(0);">
                                                <i class="bi bi-check-lg"></i>
                                            </button>`;
                            return status;
                        },
                    }
                ]
            });

            $('body').on('click', '.acceptUser', function () {

                var userId = $(this).data("id");
                console.log(userId);
                confirm("Are You sure want to Accept User ?");

                $.ajax({
                    type: "PUT",
                    url: "{{ url('user') }}/acceptUser/"+userId,
                    success: function (data) {
                        table.draw();
                        totalUser();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });
        // chartjs
        var xValues = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Today'];
        var jumlahToko = document.getElementById("jumlahTokoChart ").getContext("2d"); 
        var jumlahProduk = document.getElementById("jumlahProdukChart").getContext("2d"); 
        var totalUser = document.getElementById("totalUserChart").getContext("2d"); 
        var gradient = jumlahToko.createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, 'rgba(250,174,50,1)');   
        gradient.addColorStop(1, 'rgba(250,174,50,0)');

        var options = {
            responsive: true,
            datasetStrokeWidth : 0,
            pointDotStrokeWidth : 0,
            tooltipFillColor: "rgba(0,0,0,0.8)",
            tooltipFontStyle: "bold",
            scales: {
                xAxes: [{
                    gridLines: {
                        display: true,
                        circular : true,
                        color : "#dddddd",
                        borderDash : [5, 5, 5],
                        lineWidth : 1,
                        zeroLineWidth : 1,
                        zeroLineColor : "#dddddd",
                        zeroLineBorderDash : [5, 5, 5],

                    },
                    ticks: {
                        display: true //this will remove only the label
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        display: false //this will remove only the label
                    }
                }]
            },
            tooltips: {                
            },
        };
        
        var jumlahTokoChart = new Chart(jumlahToko,{
            type : "line",
            data : {
                labels : xDateStore,
                datasets: [
                    {
                        label: 'Dataset Store',
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        borderColor: "#ff0000", // ganti warna border line
                        data : yDataStore,
                        pointBackgroundColor : "transparent",
                        pointBorderColor : "transparent",
                        pointBorderWidth : 3,
                        // pointHitRadius : 10,
                        pointHoverRadius : 8,
                        pointHoverBackgroundColor : "#ff0000",
                        pointHoverBorderColor : "#ffffff",
                        pointHoverBorderWidth : 7,
                        spanGaps : true
                    }
                ]
                },
            options : options
        });
        var jumlahProdukChart = new Chart(jumlahProduk,{
            type : "line",
            data : {
                labels : xDateProduct,
                datasets: [
                    {
                        label: 'Dataset Product',
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        borderColor: "#ff0000", // ganti warna border line
                        data : yDataProduct,
                        pointBackgroundColor : "transparent",
                        pointBorderColor : "transparent",
                        pointBorderWidth : 3,
                        // pointHitRadius : 10,
                        pointHoverRadius : 8,
                        pointHoverBackgroundColor : "#ff0000",
                        pointHoverBorderColor : "#ffffff",
                        pointHoverBorderWidth : 7,
                        spanGaps : true
                    }
                ]
            },
            options : options
        });
        var totalUserChart = new Chart(totalUser,{
            type : "line",
            data : {
                labels : xDateUser,
                datasets: [
                    {
                        label: 'Dataset User',
                        backgroundColor : gradient, // Put the gradient here as a fill color
                        borderColor: "#ff0000", // ganti warna border line
                        data : yDataUser,
                        pointBackgroundColor : "transparent",
                        pointBorderColor : "transparent",
                        pointBorderWidth : 3,
                        // pointHitRadius : 10,
                        pointHoverRadius : 8,
                        pointHoverBackgroundColor : "#ff0000",
                        pointHoverBorderColor : "#ffffff",
                        pointHoverBorderWidth : 7,
                        spanGaps : true
                    }
                ]
                },
            options : options
        });
        //page control
        function openCity(evt, pageName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(pageName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
@endsection