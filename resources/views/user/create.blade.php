@extends('layout.layout')

@section('title', 'New User')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6">
                <h5 class="font-weight-500">New User</h5>
            </div>
        </div>
        <hr class="my-3">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped table-borderlesss" id="dataTable">
                    <thead>
                        <tr>
                            <th class="text-center py-3">No</th>
                            <th class="py-3">Nama Nasabah</th>
                            <th class="py-3">Jenis Usaha</th>
                            <th class="py-3">Rantai Usaha</th>
                            <th class="py-3">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.create') }}",
                    dataSrc: 'data.original.data'
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name', "render": function ( data, type, row, meta ) {
                        return '<a href="{{ url("/user") }}/'+row.id+'/detail">'+data+'</a>';
                        }  
                    },
                    { data: 'business_type', name: 'business_type' },
                    { data: 'business_chain', name: 'business_chain' },
                    { 
                        data: 'id',
                        name: 'action',
                        "render": function(data, type, full) {
                            let status = `<button class="btn btn-gray d-inline">
                                                <i class="bi bi-x-lg text-danger declineUser" data-id="`+data+`" href="javascript:void(0);"></i>
                                            </button>
                                            <button class="btn btn-green d-inline acceptUser" data-id="`+data+`" href="javascript:void(0);">
                                                <i class="bi bi-check-lg"></i>
                                            </button>`;
                            return status;
                        },
                    }
                ]
            });

            $('body').on('click', '.declineUser', function () {
     
                var userId = $(this).data("id");
                console.log(userId);
                
                if (confirm("Are You sure want to decline User ?")) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('user') }}/decline/"+userId,
                        success: function (data) {
                            table.draw();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });   
                }
            });

            $('body').on('click', '.acceptUser', function () {
     
                var userId = $(this).data("id");
                console.log(userId);
                if (confirm("Are You sure want to Accept User ?")) {
                    $.ajax({
                        type: "PUT",
                        url: "{{ url('user') }}/acceptUser/"+userId,
                        success: function (data) {
                            table.draw();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });
        });
    </script>
@endsection