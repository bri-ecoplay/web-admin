@extends('layout.layout')

@section('title', 'All User')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6">
                <h5 class="font-weight-500">All User</h5>
            </div>
        </div>
        <hr class="my-3">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-stripped table-borderless" id="dataTable">
                    <thead>
                        <tr>
                            <th class="text-center py-3">No</th>
                            <th class="py-3">Nama Nasabah</th>
                            <th class="py-3">Nama Usaha</th>
                            <th class="py-3">Jenis Usaha</th>
                            <th class="py-3">Rantai Usaha</th>
                            <th class="py-3">Provinsi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.index') }}",
                    dataSrc: 'data.original.data'
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'business_name', name: 'business_name' },
                    { data: 'business_type', name: 'business_type' },
                    { data: 'business_chain', name: 'business_chain' },
                    { data: 'province', name: 'province' }
                ]
            });
        });
    </script>
@endsection