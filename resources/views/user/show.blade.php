@extends('layout.layout')

@section('title', 'Detail User')

@section('style')
    <style>
        
    </style>
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-6">
                <h5>Detail User</h5>
            </div>
        </div>
        <hr class="my-3">
        <div class="row">
            <div class="col-lg-12">
                <div class="tab">
                    <a href="javascript:void(0)" class="tablinks active" onclick="openPage(event, 'dataDiri')">Data Diri</a>
                    <a href="javascript:void(0)" class="tablinks" onclick="openPage(event, 'infoToko')">Info Toko</a>
                    <a href="javascript:void(0)" class="tablinks" onclick="openPage(event, 'filePendukung')">File Pendukung</a>
                </div>
                <div id="dataDiri" class="tabcontent" style="display: block">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $detailNasabah->fullname }}</td>
                                </tr>
                                <tr>
                                    <td>Username</td>
                                    <td>{{ $detailNasabah->username }}</td>
                                </tr>
                                <tr>
                                <td>Email</td>
                                    <td>{{ $detailNasabah->email }}</td>
                                </tr>
                                <tr>
                                    <td>Rekening BRI</td>
                                    <td>{{ $detailNasabah->accountInfo->account_number }}</td>
                                </tr>
                                <tr>
                                    <td>Nomor HP</td>
                                    <td>{{ $detailNasabah->phone_number }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="infoToko" class="tabcontent">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td>Nama Usaha</td>
                                    <td>{{ $detailNasabah->detailBusiness->business_name }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Usaha</td>
                                    <td>{{ $detailNasabah->detailBusiness->business_type }}</td>
                                </tr>
                                <tr>
                                    <td>Rantai Usaha</td>
                                    <td>{{ $detailNasabah->detailBusiness->business_chain }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Usaha</td>
                                    <td>{{ $detailNasabah->business_data->business_address }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="filePendukung" class="tabcontent">
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="table table-borderless">
                                <tr>
                                    <td>File SIUP</td>
                                    <td><a href="{{ $detailNasabah->user_file->siup }}">siup.png</a></td>
                                </tr>
                                <tr>
                                    <td>File TDP</td>
                                    <td><a href="{{ $detailNasabah->user_file->tdp }}">tdp.png</a></td>
                                </tr>
                                <tr>
                                    <td>File NPWP</td>
                                    <td><a href="{{ $detailNasabah->user_file->npwp }}">npwp.png</a></td>
                                </tr>
                                <tr>
                                    <td>Berkas Pendukung Lain</td>
                                    <td><a href="{{ $detailNasabah->user_file->support_file }}">lain.png</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-5">
        <div class="row">
            <div class="col-lg-12" style="float: left;">
                <a href="{{ url('user') }}/acceptUser/{{ $id }}" class="btn btn-primary btn-lg">Setujui</a>
                <a href="javascript:void(0);" class="btn text-danger bg-grey btn-lg" onclick="deleteProfile('{{ $id }}');">Tolak</a>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteProfile(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if (confirm("Are You sure want to decline User ?")) {
                console.log("here");
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('user') }}/decline/"+id,
                    success: function (data) {
                        console.log('succes:', data);
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
                window.location = "{{ url('user/create') }}";
            }
        }
        function openPage(evt, pageName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(pageName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
@endsection