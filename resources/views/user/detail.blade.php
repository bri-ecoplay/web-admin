@extends('layout.layout')

@section('title', 'Detail User')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-6">
            <h5>Detail User</h5>
        </div>
    </div>
    <hr class="my-3">
    <div class="row">
        <div class="col-lg-12">
            <div class="tab">
                <a href="javascript:void(0)" class="tablinks active" onclick="openPage(event, 'dataNasabah')">Data Nasabah</a>
                <a href="javascript:void(0)" class="tablinks" onclick="openPage(event, 'dataToko')">Data Toko</a>
              </div>
              <div id="dataNasabah" class="tabcontent" style="display: block">
                  <div class="row">
                      <div class="col-lg-8">
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Data Personal</p></td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>{{ $detailNasabah->fullname }}</td>
                            </tr>
                            <tr>
                                <td>Username</td>
                                <td>{{ $detailNasabah->username }}</td>
                            </tr>
                            <tr>
                                <td>No Handphone</td>
                                <td>{{ $detailNasabah->phone_number }}</td>
                            </tr>
                            <tr>
                                <td>No Whatsapp</td>
                                <td>{{ $detailNasabah->whatsapp_number }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ $detailNasabah->email }}</td>
                            </tr>
                            <tr>
                                <td>Nomor KTP</td>
                                <td>{{ $detailNasabah->id_number }}</td>
                            </tr>
                        </table>
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Informasi Berkas</p></td>
                            </tr>
                            <tr>
                                <td>File SIUP</td>
                                <td><a href="{{ $detailNasabah->user_file->siup }}">siup.png</a></td>
                            </tr>
                            <tr>
                                <td>File TDP</td>
                                <td><a href="{{ $detailNasabah->user_file->tdp }}">tdp.png</a></td>
                            </tr>
                            <tr>
                                <td>File NPWP</td>
                                <td><a href="{{ $detailNasabah->user_file->npwp }}">npwp.png</a></td>
                            </tr>
                            <tr>
                                <td>Berkas Pendukung Lain</td>
                                <td><a href="{{ $detailNasabah->user_file->support_file }}">lain.png</a></td>
                            </tr>
                        </table>
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Informasi Bank</p></td>
                            </tr>
                            <tr>
                                <td>Nama Bank</td>
                                <td>{{ $detailNasabah->accountInfo->bank_account }}</td>
                            </tr>
                            <tr>
                                <td>Nomor Rekening</td>
                                <td>{{ $detailNasabah->accountInfo->account_number }}</td>
                            </tr>
                            <tr>
                                <td>Nama Pemilik Rekening</td>
                                <td>{{ $detailNasabah->accountInfo->account_name }}</td>
                            </tr>
                        </table>
                      </div>
                      <div class="col-lg-4">
                        <img src="{{ asset('assets/img/avatar/avatar-1.png') }}" style="width: 100%">
                    </div>
                  </div>
              </div>
              <div id="dataToko" class="tabcontent">
                <div class="row">
                    <div class="col-lg-8">
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Informasi Toko</p></td>
                            </tr>
                            <tr>
                                <td>Nama Usaha</td>
                                <td>{{ $detailNasabah->detailBusiness->business_name }}</td>
                            </tr>
                            <tr>
                                <td>Jenis Usaha</td>
                                <td>{{ $detailNasabah->detailBusiness->business_type }}</td>
                            </tr>
                            <tr>
                                <td>Rantai Usaha</td>
                                <td>{{ $detailNasabah->detailBusiness->business_chain }}</td>
                            </tr>
                            <tr>
                                <td>Deskripsi Toko</td>
                                <td>{{ $detailNasabah->detailBusiness->business_description }}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Karyawan</td>
                                <td>{{ $detailNasabah->detailBusiness->total_employee }}</td>
                            </tr>
                            <tr>
                                <td>Omset Perbulan</td>
                                <td>{{ $detailNasabah->detailBusiness->monthly_income }}</td>
                            </tr>
                        </table>
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Social Media</p></td>
                            </tr>
                            <tr>
                                <td>Facebook</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->facebook }}">facebook</a></td>
                            </tr>
                            <tr>
                                <td>Instagram</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->instagram }}">instagram</a></td>
                            </tr>
                            <tr>
                                <td>Web</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->website }}">website</a></td>
                            </tr>
                            <tr>
                                <td>Shopee</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->shopee }}">shopee</a></td>
                            </tr>
                            <tr>
                                <td>Tokopedia</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->tokopedia }}">tokopedia</a></td>
                            </tr>
                            <tr>
                                <td>Bukalapat</td>
                                <td><a href="{{ $detailNasabah->detailBusiness->bukalapak }}">bukalapak</a></td>
                            </tr>
                        </table>
                        <table class="table table-borderless">
                            <tr>
                                <td colspan="2"><p style="font-weight: bold">Informasi Alamat Toko</p></td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>{{ $detailNasabah->business_data->province_text }}</td>
                            </tr>
                            <tr>
                                <td>Kabupaten/Kota</td>
                                <td>{{ $detailNasabah->business_data->city_district_text }}</td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>{{ $detailNasabah->business_data->district_text }}</td>
                            </tr>
                            <tr>
                                <td>Pos</td>
                                <td>{{ $detailNasabah->business_data->postcode }}</td>
                            </tr>
                            <tr>
                                <td>Detail Alamat</td>
                                <td>{{ $detailNasabah->business_data->business_address }}</td>
                            </tr>
                            <tr>
                                <td>Pin Lokasi</td>
                                <td><a href="{{ $detailNasabah->business_data->pin_location }}">{{ $detailNasabah->business_data->pin_location }}</a></td>
                            </tr>
                        </table>
                      </div>
                      <div class="col-lg-4">
                        <img src="{{ asset('assets/img/avatar/avatar-1.png') }}" style="width: 100%">
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <hr class="my-5">
    <div class="row">
        <div class="col-lg-12" style="float: left;">
            <a href="{{ route('user.show', $id) }}" class="btn text-danger bg-grey btn-lg">Hapus</a>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        function openPage(evt, pageName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(pageName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
@endsection