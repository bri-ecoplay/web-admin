@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Artikel</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <form action="{{ url('/artikel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-2">
                            <label for="input-file">
                                <div class="input-img">
                                    <img src="{{ asset ('svg/img.svg')}}" alt="">
                                    <p class="upload-text">Upload</p>
                                </div>
                            </label>
                        </div>
                        <!-- hidden button -->
                        <div class="col-2" style="margin-top: 20px;">
                            <input type="file" class="input" id="input-file" name="image">
                        </div>
                    </div>
                    <div class="create-event mt-40">
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Judul Artikel</label>
                            <input type="text" class="input-text" id="" name="title" placeholder="Contoh : Promo BRIFICE">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Artikel</label>
                            <textarea name="" class="input-textarea" id="" cols="30" rows="25" name="content" placeholder="Contoh : promo.bri.co.id/produc/.."></textarea>
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>

        <!-- List Banner Section -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500 mt-70">List Artikel</h5>
            </div>
            <div class="col-lg-12">
                <div class="mt-20">
                    <table class="table table-striped table-borderless" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center py-3">No</th>
                                <th class="py-3">Update</th>
                                <th class="py-3">Penulis</th>
                                <th class="py-3">Judul</th>
                                <th class="py-3">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    function formatDate(date) {
        const transMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'];

        let d = new Date(date);

        let month = transMonth[d.getMonth()],
            day = d.getDate(),
            year = d.getFullYear(),
            hours = d.getHours(),
            minutes = d.getMinutes();

        if (day.length < 2)
            day = '0' + day;

        return `${year} ${month} ${day} ; ${hours} : ${minutes}`;
    }

    $(document).ready(function() {
        $('#dataTable').DataTable({
            "pageLength": 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('artikel.index') }}",
                dataSrc: 'data.original.data'
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: "date",
                    name: "date",
                    "render": function(data, type, full) {
                        let date = formatDate(data)
                        return date;
                    }
                },
                {
                    data: 'admin_id',
                    name: 'admin_id',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'title',
                    name: 'title',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'id',
                    name: 'action',
                    "render": function(data, type, full) {
                        let action = `<a href="artikel/${data}/edit" class="btn btn-gray d-inline">
                                        Edit
                                    </a>`;
                        return action;
                    }
                }
            ]
        });
    });
</script>
@endsection