@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Artikel</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <form action="{{ url('/artikel', $dataNews->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-2">
                            <label for="input-file">
                                <div class="input-img">
                                    <img src="{{ $dataNews->thumbnail }}" alt="">
                                    <p class="upload-text">Upload</p>
                                </div>
                            </label>
                        </div>
                        <!-- hidden button -->
                        <div class="col-2" style="margin-top: 20px;">
                            <input type="file" class="input" id="input-file" name="image">
                        </div>
                    </div>
                    <div class="create-event mt-40">
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Judul Artikel</label>
                            <input type="text" class="input-text" id="" name="title" value="{{ $dataNews->title }}">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Artikel</label>
                            <textarea name="" class="input-textarea" id="" cols="30" rows="25" name="content">
                            {{ $dataNews->content??'' }}
                            </textarea>
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function() {

    });
</script>
@endsection