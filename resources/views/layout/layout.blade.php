<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    @yield('style')
    <title>Ecoplay &mdash; @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('img/logo.png') }}" class="rounded-circle" height="50" width="50" />
                    <span id="logo">Ecoplay</span>
                </a>
                <div class="navbar-nav ms-auto fw-bold">
                    <ul class="list-group list-group-horizontal">
                        <li class="list-group-item bg-transparent border-0">
                            <p class="mt-2 ">{{Session::get('userName')}}</p>
                        </li>
                        <li class="list-group-item bg-transparent border-0">
                            <img src="{{ asset('img/user.png') }}" height="50" width="50" alt="">
                        </li>
                        <li class="list-group-item bg-transparent border-0">
                            <form action="{{ url('/logout') }}" class="form-horizontal" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <button class="btn btn-primary btn-lg btn-icon icon-right text-uppercase" tabindex="4">
                                        Logout
                                    </button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <!-- main content -->

    <main id="main-content">
        <div style="background-color: #e2e2e2">
            <div class="container bg-white">
                <div class="row">
                    <div class="col-lg-3 border-end border-1">
                        <div class="container">
                            <div id="sidebar-menu" class="d-inline">
                                <br>
                                <ul class="list-group">
                                    <!-- dashboard -->
                                    <li class="list-group-item mb-2 {{ request()->is('dashboard') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0">
                                        <a href="{{ route('dashboard') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('dashboard'))
                                            <img src="{{ asset('svg/dashboard1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/dashboard0.svg') }}" alt="">
                                            @endif
                                            <span>Dashboard</span>
                                        </a>
                                    </li>

                                    <!-- new user -->
                                    <li class="list-group-item mb-2 {{ request()->is('user/create') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{ route('user.create') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('user/create'))
                                            <img src="{{ asset('svg/user1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/user0.svg') }}" alt="">
                                            @endif
                                            <span>New User</span>
                                        </a>
                                    </li>

                                    <!-- all user -->
                                    <li class="list-group-item mb-2 {{ request()->is('user') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{ route('user.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('user'))
                                            <img src="{{ asset('svg/all-user1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/all-user0.svg') }}" alt="">
                                            @endif
                                            <span>All User</span>
                                        </a>
                                    </li>

                                    <!-- Administrator -->
                                    @if(Session::get('userRole') == 1)
                                    <li class="list-group-item mb-2 {{ request()->is('administrator') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{  route('administrator.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('administrator'))
                                            <img src="{{ asset('svg/admin1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/admin0.svg') }}" alt="">
                                            @endif
                                            <span>Administrator</span>
                                        </a>
                                    </li>
                                    @endif

                                    <!-- Event -->
                                    <!-- <li class="list-group-item mb-2 {{ request()->is('event') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{  route('event.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('event'))
                                            <img src="{{ asset('svg/event1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/event0.svg') }}" alt="">
                                            @endif
                                            <span>Event</span>
                                        </a>
                                    </li> -->

                                    <!-- Artikel -->
                                    <li class="list-group-item mb-2 {{ request()->is('artikel') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{  route('artikel.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('artikel'))
                                            <img src="{{ asset('svg/artikel0.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/artikel1.svg') }}" alt="">
                                            @endif
                                            <span>Artikel</span>
                                        </a>
                                    </li>

                                    <!-- all banner -->
                                    <li class="list-group-item mb-2 {{ request()->is('banner') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{ route('banner.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('banner'))
                                            <img src="{{ asset('svg/artikel0.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/artikel1.svg') }}" alt="">
                                            @endif
                                            <span>Banner</span>
                                        </a>
                                    </li>

                                    <!-- wa group -->
                                    <li class="list-group-item mb-2 {{ request()->is('wa-group') ? 'border-start border-3 border-primary text-blue-link' : '' }} border-0 ">
                                        <a href="{{ route('wa-group.index') }}" class="text-decoration-none text-secondary">
                                            @if(request()->is('wa-group'))
                                            <img src="{{ asset('svg/all-user1.svg') }}" alt="">
                                            @else
                                            <img src="{{ asset('svg/all-user0.svg') }}" alt="">
                                            @endif
                                            <span>Wa Group</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 p-3">
                        <div id="content" class="overflow-auto vh-100">
                            <section>
                                @yield('content')
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal fade" role="dialog" id="imgmodal">
        <div class="modal-dialog">
            <div class="modal-content"></div>
            <img class="img-responsive" src="" id="show-img">
        </div>
    </div>
    <!-- footer  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    <!-- <script src="js/index.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>

    <!-- chart link script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script>
        var getUserToken = localStorage.getItem('userToken');
        //TODOS : REMOVE THIS CONSOLE GET USER TOKEN
        console.log(`this is your token :  ${getUserToken}`)

        $(document).ready(function() {
            $(document).on('click', '#main-content img', function() {
                var img = $(this).attr('src');
                $("#show-img").attr('src', img);
                $("#imgmodal").modal('show');
            });
        });
    </script>
    @yield('script')
</body>

</html>