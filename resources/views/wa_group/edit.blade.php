@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Wa Group</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <div class="note">
                    <p class="note-text"><span>Catatan : </span>Silakan upload foto wa group dengan ukuran 308 x 128, dengan format JPG, JPEG dan PNG,</p>
                </div>

                <form action="{{ url('/wa-group', $dataWa->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="create-event mt-20">
                        <div class="row">
                            <div class="col-2">
                                <label for="input-file">
                                    <div class="input-img">
                                        <img src="{{ asset ('svg/img.svg')}}" alt="">
                                        <p class="upload-text">Upload</p>
                                    </div>
                                </label>
                            </div>
                            <!-- hidden button -->
                            <div class="col-2" style="margin-top: 20px;">
                                <input type="file" class="input" id="input-file" name="image">
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Name</label>
                            <input type="text" class="input-text" id="" name="name" value="{{$dataWa->name}}">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Link</label>
                            <input type="text" class="input-text" id="" name="link" value="{{$dataWa->link}}">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Member</label>
                            <input type="number" class="input-text" id="" name="member" value="{{$dataWa->member}}">
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function() {

    });
</script>
@endsection