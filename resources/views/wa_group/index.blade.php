@extends('layout.layout')
@section('title','Event')
@section('content')
<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500">Wa Group</h5>
            </div>
            @if (session('success'))
            <div class="alert alert-success">
                {!! session('success') !!}
            </div>
            @endif
            <div class="col-12">
                <div class="note">
                    <p class="note-text"><span>Catatan : </span>Silakan upload foto wa group dengan ukuran 308 x 128, dengan format JPG, JPEG dan PNG,</p>
                </div>

                <form action="{{ url('/wa-group') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="create-event mt-20">
                        <div class="row">
                            <div class="col-2">
                                <label for="input-file">
                                    <div class="input-img">
                                        <img src="{{ asset ('svg/img.svg')}}" alt="">
                                        <p class="upload-text">Upload</p>
                                    </div>
                                </label>
                            </div>
                            <!-- hidden button -->
                            <div class="col-2" style="margin-top: 20px;">
                                <input type="file" class="input" id="input-file" name="image">
                            </div>
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Name</label>
                            <input type="text" class="input-text" id="" name="name" placeholder="Contoh : Group Baru">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Link</label>
                            <input type="text" class="input-text" id="" name="link" placeholder="Contoh : http://">
                        </div>
                        <div class="row">
                            <label for="" class="form-label" style="margin-top: 20px;">Member</label>
                            <input type="number" class="input-text" id="" name="member" placeholder="Contoh : 10">
                        </div>
                        <button type="submit" class="btn-submit mt-20">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>

        <!-- List Banner Section -->
        <div class="row">
            <div class="col-lg-12">
                <h5 class="font-weight-500 mt-70">List Banner</h5>
            </div>
            <div class="col-lg-12">
                <div class="mt-20">
                    <table class="table table-striped table-borderless" id="dataTable">
                        <thead>
                            <tr>
                                <th class="text-center py-3">No</th>
                                <th class="py-3">Name</th>
                                <th class="py-3">Link</th>
                                <th class="py-3" style="width:30%">Image</th>
                                <th class="py-3">Member</th>
                                <th class="py-3">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td>12 Jul 2021 ; 12:40</td>
                                <td>Promo UMKM</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            "pageLength": 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('wa-group.index') }}",
                dataSrc: 'data.original.data'
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'image',
                    name: 'image',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return `<img src="${data}" atl="banner" width="100%" class="image">`;
                    }
                },
                {
                    data: 'link',
                    name: 'link',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'member',
                    name: 'member',
                    "render": function(data, type, full) {
                        if (!data)
                            return '-';
                        return data;
                    }
                },
                {
                    data: 'id',
                    name: 'action',
                    "render": function(data, type, full) {
                        let action = `<a href="wa-group/${data}/edit" class="btn btn-gray d-inline">
                                        Edit
                                    </a>`;
                        return action;
                    }
                }
            ]
        });
    });
</script>
@endsection