<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <link rel="icon" href="{{ asset('img/logo.png') }}">
  <title>Ecoplay &mdash; Web Admin</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="../node_modules/bootstrap-social/bootstrap-social.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css ') }}">
  <link rel="stylesheet" href="{{ asset('style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/component.css ') }}">
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="d-flex flex-wrap align-items-stretch">
        <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
          <div class="p-4 m-3">
            <div class="logo-box">
              <div class="row mb-5">
                <div class="col-2">
                  <img src="{{ asset('img/logo.png') }}" height="50" width="50" />
                </div>
                <div class="col-5">
                  <h1 id="logo">Ecoplay</h1>
                </div>
              </div>
            </div>

            <h4 class="text-dark fs-1 font-weight-bold">Selamat Datang</h4>
            <p class="text-muted">Before you get started, you must login or register if you don't already have an account.</p>
            <p class="text-muted">{{ $errors->first('username') }}</p>
            <form action="{{ url('/login') }}" class="form-horizontal" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <img src="{{ asset('svg/username.svg') }}">
                    </div>
                  </div>
                  <input id="username" type="text" name="username" class="form-control" placeholder="Username" tabindex="1" required autofocus>
                </div>
              </div>

              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <img src="{{ asset('svg/password.svg') }}">
                    </div>
                  </div>
                  <input id="password" type="password" name="password" class="form-control" placeholder="Password" tabindex="1" data-indicator="pwindicator" required>
                </div>
              </div>
              <div class="form-group">
                <button class="btn btn-primary btn-lg btn-icon icon-right text-uppercase" tabindex="4">
                  Masuk
                </button>
              </div>

            </form>
          </div>
        </div>
        <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom" data-background="{{ asset('img/login_image.png')}} ">
          <div class="absolute-bottom-left index-2">
            <div class="text-light p-5 pb-2">

            </div>
          </div>
        </div>
        <p></p>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="../assets/js/stisla.js"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../assets/js/scripts.js"></script>
  <script src="../assets/js/custom.js"></script>

  <!-- Page Specific JS File -->

  <script>
    // $('#submit').click(function() {
    //   $.ajax({
    //     url: 'https://ecoplay-erwinhermanto31.cloud.okteto.net/api/v1/login_admin',
    //     type: 'POST',
    //     dataType: 'json',
    //     contentType: "application/json; charset=utf-8",
    //     data: JSON.stringify({
    //       "username": "admin",
    //       "password": "1@admin"
    //     }),
    //     success: function(respon) {
    //       var responseData = respon.data
    //       //SET USER TOKEN//
    //       //TODO'S : remove console set local token///
    //       console.log(`Your Token User is Set my Darling ${responseData.token}`)
    //       localStorage.setItem('userToken', JSON.stringify(responseData.token));
    //       /////////////////

    //       // location.href = "{{ route('dashboard') }}"
    //     },
    //     error: function(data) {
    //       console.log(data)
    //       // alert(data.responseJSON.data.message)
    //       // location.href = "{{ route('dashboard') }}"
    //     }

    //   })
    // })
  </script>
</body>

</html>