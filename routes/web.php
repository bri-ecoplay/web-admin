<?php

use App\Http\Controllers\AdministratorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\WaGroupController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index']);
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout']);
Route::group(['middleware' => 'token'], function () {
    Route::get('/dashboard', [LoginController::class, 'dashboard'])->name('dashboard');
    
    Route::resource('administrator', AdministratorController::class);
    Route::put('admin/{id}/update-status', [AdministratorController::class, 'update_status'])->name('admin.update-status');
    Route::resource('user', UserController::class);
    Route::get('dataDashboard', [UserController::class, 'dataDashboard'])->name('user.dataDashboard');
    Route::put('user/acceptUser/{id}', [UserController::class, 'acceptUser'])->name('user.acceptUser');
    Route::delete('user/decline/{id}', [UserController::class, 'destroy'])->name('user.destroy');
    Route::get('user/{id}/detail', [UserController::class, 'detail'])->name('user.detail');
    Route::resource('event', EventController::class);
    Route::resource('artikel', ArtikelController::class);
    Route::resource('banner', BannerController::class);
    Route::post('banner/{id}/update-status', [BannerController::class, 'update_status'])->name('banner.update-status');
    Route::resource('wa-group', WaGroupController::class);
});
